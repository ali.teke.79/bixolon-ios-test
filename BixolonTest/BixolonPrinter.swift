//
//  BixolonPrinter.swift
//  BixolonTest
//
//  Created by ali teke on 24/10/2018.
//  Copyright © 2018 ali teke. All rights reserved.
//

import Foundation

class BixolonPrinter: NSObject, UPOSDeviceControlDelegate  {
    private var device: UPOSPrinter?
    private var logView: UITextView?
    private var uposPrinterController: UPOSPrinterController = UPOSPrinterController()
    private let encoding = CFStringConvertEncodingToNSStringEncoding(CFStringEncoding(CFStringEncodings.windowsLatin5.rawValue))

    init(_ logView: UITextView!) {
        super.init()
        self.logView = logView
        
        uposPrinterController.setLogLevel(UInt8(LOG_SHOW_NEVER))
        uposPrinterController.characterSet = 437
        uposPrinterController.delegate = self
        
        /* Read stored devices and delete from the device list */
        let deviceList = uposPrinterController.getRegisteredDevice() as? UPOSPrinters;
        while(deviceList?.getList().count != 0) {
             deviceList?.removeDevice(deviceList?.getList().last);
        }

        /* Get first paired device */
        device = deviceList?.getPairedDevices().first as? UPOSPrinter;
        if (device == nil) {
            addLog(message: "No paired device found!")
            return
        }
        
        /* Add device to the device list and save it */
        deviceList?.addDevice(device);
        deviceList?.save();
    
        /* Claim and enable device */
        uposPrinterController.claim(5000)
        uposPrinterController.deviceEnabled = true
        
        /* Connect to device */
        uposPrinterController.connect(device?.modelName)
        if (uposPrinterController.state != Int(__UPOS_STATES.UPOS_S_CLOSED.rawValue)) {
            addLog(message: "Printer open")
        }
    }
    
    func testCpcl() {
        var str: String = ""
        str.append("! UTILITIES \r\n")
        str.append("BAR-SENSE 82 \r\n")
        str.append("IN-DOTS \r\n")
        str.append("ON-FEED FEED \r\n")
        str.append("COUNTRY LATIN9 \r\n")
        str.append("SET-TOF 30 \r\n")
        str.append("PRINT \r\n")
        str.append("! 0 200 200 1000 1 \r\n")
        str.append("LABEL \r\n")
        str.append("CONTRAST 0 \r\n")
        str.append("TONE 4 \r\n")
        str.append("SPEED 5 \r\n")
        str.append("PAGE-WIDTH 585 \r\n")
        str.append(";// PAGE 0000000005761120 \r\n")
        str.append(";//{{BEGIN-BT \r\n")
        str.append("T 5 0 0 340 ĞğÜüŞşİiÖöÇç \r\n")
        str.append("T 5 0 0 358 XXXXXXXXXXXXXXXXXX \r\n")
        str.append("T 5 0 0 376 XXXXXXXXXXXXXXXXXX \r\n")
        str.append("T 5 0 0 394 XXXXXXXXXXXXXXXXXX \r\n")
        str.append("T 5 0 0 412 XXXXXXXXXXXXXXXXXX \r\n")
        str.append(";//{{END-BT \r\n")
        str.append("FORM \r\n")
        str.append("PRINT \r\n")
        
        uposPrinterController.printRawData(Int(__UPOS_PRINTER_STATION.PTR_S_RECEIPT.rawValue), data: str.data(using: String.Encoding(rawValue: encoding)))
    }
    
    func testBarcode() {
        uposPrinterController.printBarcode(Int(__UPOS_PRINTER_STATION.PTR_S_RECEIPT.rawValue), data: "12345678",
            symbology: Int(__UPOS_PRINTER_BARCODE_SYMBOLOGIES.PTR_BCS_Code128.rawValue), height: 100, width: 100,
            alignment: -2 /*Int(__UPOS_PRINTER_BARCODE_ALIGNMENT.PTR_BC_CENTER.rawValue)*/,
            textPostion: -11 /*Int(__UPOS_PRINTER_BARCODE_TEXT_POSITION.PTR_BC_TEXT_NONE.rawValue)*/)
    }
    
    @objc(StatusUpdateEvent:) func statusUpdateEvent(_ status: NSNumber) {
        var statusText: String!
        switch(UInt(status.intValue))
        {
        case __UPOS_PRINTER_STATUS.PTR_SUE_COVER_OPEN.rawValue:
            statusText = "Cover open"
            break;
        case __UPOS_PRINTER_STATUS.PTR_SUE_COVER_OK.rawValue:
            statusText = "Cover ok"
            break;
        case __UPOS_PRINTER_STATUS.PTR_SUE_REC_EMPTY.rawValue:
            statusText = "Paper empty"
            break;
        case __UPOS_PRINTER_STATUS.PTR_SUE_REC_PAPEROK.rawValue:
            statusText = "Paper ok"
            break;
        case __UPOS_PRINTER_STATUS.PTR_SUE_REC_NEAREMPTY.rawValue:
           statusText = "Paper near end"
            break;
        case __UPOS_STATUS.UPOS_SUE_POWER_OFF.rawValue:
            statusText = "Power off"
            break
        case __UPOS_STATUS.UPOS_SUE_POWER_OFF_OFFLINE.rawValue:
            statusText = "Power off offLine"
            break
        case __UPOS_STATUS.UPOS_SUE_POWER_OFFLINE.rawValue:
            statusText = "Power offLine"
            break;
        case __UPOS_STATUS.UPOS_SUE_POWER_ONLINE.rawValue:
             statusText = "Device onLine"
            break;
        case __UPOS_PRINTER_STATUS.PTR_SUE_REC_BATTERY_NORMAL.rawValue:
            statusText = "Battery state normal"
            break;
        case __UPOS_PRINTER_STATUS.PTR_SUE_REC_BATTERY_LOW.rawValue:
             statusText = "Battery state low"
            break;
        default:
            statusText = "Unknown!"
        }
        addLog(message: NSString(format:"[StatusUpdateEvent] = %@", statusText) as String);
    }
    
    func addLog(message: String) {
        objc_sync_enter(logView)
        logView.insertText(message + "\n");
        defer {
            objc_sync_exit(logView)
        }
    }
}
